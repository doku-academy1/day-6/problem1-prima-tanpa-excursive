import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(primeNumber(4));
    }

    public static Integer primeNumber(Integer number) {
        List<Integer> prime = new ArrayList<>();
        int num = 1, count = 0, i;

        while (count < number) {
            num += 1;

            for (i = 2; i <= num; i++) {
                if (num % i == 0) {
                    break;
                }
            }

            if (i == num) {
                count += 1;
                prime.add(count-1, num);
            }
        }
        return prime.get(number-1);
    }
}